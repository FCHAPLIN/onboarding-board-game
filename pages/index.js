import styles from '../styles/Home.module.scss'
import Layout from "../components/Layout/layout";
import Link from 'next/link'
import Button from "../components/Button/button";

export default function Home() {
    return (

        <Layout>
            <p className={styles.description}>
                Welcome to the
            </p>
            <h1 className={styles.title}>
                 Onboarding Board Game
            </h1>
            <p className={styles.description}>
                This is an example of a connected app with the Kenpath API. Kenpath is a service to create, share and
                play virtual paths for any purposes.
                Get more informations in the <Link href="/about">About page</Link>.
            </p>
            <Button text={'Let\'s play!'} href={'/game'} type={''}/>

        </Layout>
    )
}
