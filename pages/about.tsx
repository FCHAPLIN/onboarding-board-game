import React from 'react';
import Layout from "../components/Layout/layout";

const AboutView = () => {
    return (
        <Layout>
            <div>
                This is the about page
            </div>
        </Layout>
    );
};

export default AboutView;
