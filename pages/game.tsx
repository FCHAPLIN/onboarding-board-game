import React from 'react';
import GameContainer from "../containers/game-container";
import Layout from "../components/Layout/layout";

const GameView = () => {
    return (
        <>
            <Layout>
                <GameContainer/>
            </Layout>
        </>
    );
};

export default GameView;
