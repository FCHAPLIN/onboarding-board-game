export interface IUserInterface{
    loading: boolean;
    backgroundScroll: string;
}

export enum BackgroundScroll {
    TOP = 'TOP',
    DOWN = 'DOWN'
}
