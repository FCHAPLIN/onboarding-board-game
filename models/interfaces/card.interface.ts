export interface ICard {
    title: string;
    type: string;
    text: string;
    image: string;
    points: number;
}
