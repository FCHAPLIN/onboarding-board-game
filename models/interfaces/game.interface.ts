import {ICard} from "./card.interface";
import {ITile} from "./tile.interface";


export interface IGame{
    started: boolean,
    score: number,
    position: number,
    card: ICard
    targetPosition: number
    tiles: Array<ITile>
}
