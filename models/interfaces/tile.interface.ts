export interface ITile {
    type: string;
}

export enum TileTypes {
    QUIZ = 'QUIZ',
    FACT = 'FACT',
    EMPTY = 'EMPTY',
    ACTION = 'ACTION',
    ACTOR = 'ACTOR'
}
