import React from 'react';
import Image from 'next/image'
import style from "./Pawn.module.scss";

const Pawn = () => {
    return (
        <div className={style.pawn}>
            <Image
                src="/pion2.png"
                alt="Pawn"
                width={50}
                height={80}
            />
        </div>
    );
};

export default Pawn;

