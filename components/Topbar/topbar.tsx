import React from 'react';
import Button from "../Button/button";
import styles from './Topbar.module.scss'
import {useRouter} from "next/router";
import {useSelector} from 'react-redux'

const Topbar = () => {
    const {score} = useSelector(state => state.game)
    const router= useRouter()
    const route= router.route;
    const back = () => {
        router.back()
    }
    return (
        <>
            <div className={styles['top-bar']}>
                <div className="left">
                    { route !== '/' && (
                        <Button text={'Back'} buttonType={'transparent'} click={back}/>
                    )}
                </div>
                { route === '/game' && (
                    <div className={styles.score}>{score}Pts</div>
                )}
                <div className="right">
                    { route !== '/about' && (
                        <Button text={'About'} href={'/about'} buttonType={'transparent'}/>
                    )}

                </div>
            </div>
            <hr/>
        </>
    );
};

export default Topbar;
