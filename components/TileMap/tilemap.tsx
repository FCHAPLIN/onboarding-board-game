import React, {useEffect, useReducer, useRef, useState} from 'react';
import style from './Tilemap.module.scss'
import {useDispatch, useSelector} from "react-redux";
import Tile from "../Tile/tile";
import {ITile} from "../../models/interfaces/tile.interface";

const TileMap = (props) => {
    const {tiles, targetPosition} = useSelector(state => state.game)
    const [left, setLeft] = useState(0)
    const [top, setTop] = useState(0)
    const tilemapElement = useRef()
    const centerTile = (position) => {
        // @ts-ignore
        const width = tilemapElement.current.offsetWidth
        // @ts-ignore
        const height = tilemapElement.current.offsetHeight

            setLeft((width * .5) - position.left - 54)
            setTop(0 - (height * .5) + position.bottom)

    }

    const getPosition = (index: number) => {
        const origin = {left: 0, bottom: 0}
        let offset = {...origin}
        if (index < 8) offset =  {left: props.position * 75, bottom: props.position * 35}
        if (index >= 8 && index < 16) offset = {left: 450 - (props.position - 8) * 75, bottom: props.position * 35}
        if (index >= 16 && index < 24) offset = {
            left: 450 - (props.position - 8) * 75,
            bottom: 770 - (props.position - 8) * 35
        }
        if (index >= 24 && index < 32) offset = {
            left: -1200 + (props.position - 16) * 75,
            bottom: 770 - (props.position - 8) * 35
        }
        return {left : origin.left + offset.left, bottom: origin.bottom + offset.bottom}
    }

    useEffect(
        () => {
            centerTile(getPosition(props.position))
        },
        [props.position])

    useEffect(() => {
        function handleResize() {
            centerTile(getPosition(props.position))
        }
        window.addEventListener('resize', handleResize)
    })
    return (
        <div className={style.tileMap}
             style={{transform: 'translate( ' + left + 'px,' + top + 'px)'}}
             ref={tilemapElement}
        >
            {
                tiles.map((tile: ITile, index: number) => (
                    <Tile key={index} type={tile.type.toLowerCase()} position={index} click={centerTile}
                          getPosition={getPosition} isDestination={targetPosition === index}/>
                ))
            }
        </div>
    );
};

export default TileMap;
