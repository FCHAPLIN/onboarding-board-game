import styles from './GameBackground.module.scss'

import React from 'react';

const GameBackground = () => {
    return (
        <>
            <div className={styles.sky}>
                <div className={styles.cloud}/>
                <div className={`${styles.hill} ${styles['hill-left']}`}/>
                <div className={`${styles.hill} ${styles['hill-right']}`}/>
                <div className={`${styles.hill} ${styles['hill-center']}`}/>
            </div>
        </>
    );
};

export default GameBackground;
