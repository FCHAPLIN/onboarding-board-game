import React from 'react';
import style from './Card.module.scss'
import Image from 'next/image'
import Button from "../Button/button";
import styles from "../Card/Card.module.scss";


const Card = (props) => {
    const cardStyle = props.cardType ? styles[`card--${props.cardType}`] : styles.card
    return (
        <div className={cardStyle}>
            <div className={style.header}>
                <div className={style.point}>{props.score}</div>
                <div className={style.type}>{props.cardType}</div>
                <div className={style.point}>{props.score}</div>
            </div>
            <div className={style.image}>
                <Image
                    src={props.image}
                    alt="/nextJs logo"
                    width={300}
                    height={180}/>
            </div>
            <div className={style.content}>

            <div className={style.title}>{props.title}</div>
            <div className={style.text}>{props.text}</div>
            </div>
            <div className={style.buttons}>
                <Button click={() => props.close(props.score)} buttonType={'circle'} icon={'times'}/>
            </div>
        </div>
    );
};

export default Card;
