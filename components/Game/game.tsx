import style from './Game.module.scss'
import React, {useEffect, useState} from 'react';

import {useDispatch, useSelector} from 'react-redux'
import Button from "../Button/button";
import {TileTypes} from "../../models/interfaces/tile.interface";
import TileMap from "../TileMap/tilemap";
import Card from "../Card/card";

import {ICard} from "../../models/interfaces/card.interface";
import {cards} from "../../data";
import Pawn from "../Pawn/pawn";

const Game = () => {
    const dispatch = useDispatch()
    const [dice, setDice] = useState(null)
    const {position, tiles, targetPosition, card} = useSelector(state => state.game)

    const getCard = (type: string):ICard => {
        const cardType = type.toLowerCase();
        const cardIndex = Math.floor(Math.random()* cards[cardType].length);
        return cards[cardType][cardIndex]
    }
    const closeCard = (score: number) => {
        dispatch({ type: 'CLOSE_CARD', payload: score})
    }

    const rollDice = () => {
        const result = Math.floor(Math.random()*6) +1;
        setDice(result)
        dispatch({ type: 'SET_TARGET_POSITION', payload: result })
    }
    useEffect(
        () => {
            let timer = setTimeout(() => {
                let nextPosition = position + 1
                nextPosition = nextPosition >= tiles.length ? 0 : nextPosition
                dispatch({ type: 'PLAYER_ADVANCE', payload: nextPosition })
            }, 500)
            if (position === targetPosition) {
                setDice(null)
                clearTimeout(timer)
                if(tiles[position].type !== TileTypes.EMPTY){
                    dispatch({ type: 'DRAW_CARD', payload: getCard(tiles[position].type)})
                }
            }
            return () => {
                clearTimeout(timer)
            }
        },
        [position, targetPosition]
    )
    return (
        <div className={style.game}>
            <Pawn/>
            <TileMap position={position}/>
            <div className={style.dice}>
                <Button text={dice ? dice  : 'Roll the dice'} click={rollDice}/>
            </div>
            {card && (
                <Card title={card.title} text={card.text} image={card.image} cardType={card.type.toLowerCase()} score={card.points} close={closeCard}/>
            )}
        </div>
    );
};

export default Game;
