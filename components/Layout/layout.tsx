import React from 'react';
import Head from "next/head";
import styles from '../../styles/Home.module.scss'
import GameBackground from "../GameBackground/game-background";
import Footer from "../Footer/footer";
import Topbar from "../Topbar/topbar";

const Layout = (props) => {
    return (
        <div className={styles.container}>
            <Head>
                <title>Onboarding Board Game</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
        <main className={styles.main}>
            <Topbar/>
            {props.children}
            <GameBackground/>
        </main>
            <Footer/>
            </div>
    );
};

export default Layout;
