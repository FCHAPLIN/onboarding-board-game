import style from './Tile.module.scss'
import React, {useMemo} from 'react';

const Tile = (props) => {
    const getPosition = (index: number) => {
        if (index < 8) return {left: index * 75, bottom: index * 35  }
        if (index >= 8 && index < 16) return {left: 450 - (index - 8) * 75, bottom: index * 35 }
        if (index >= 16 && index < 24) return {left: 450 - (index - 8) * 75, bottom: 770 - (index - 8) * 35 }
        if (index >= 24 && index < 32) return {left: -1200 + (index - 16) * 75, bottom: 770 - (index - 8) * 35 }
    }
    const xyPosition = useMemo(() => getPosition(props.position), [props.position])
    const tileStyle = props.type ? style[`tile--${props.type}`] : style.tile

    return (
        <div className={tileStyle} style={xyPosition} onClick={() => props.click(xyPosition)}>
            {props.isDestination && (
            <div className={style.destination}/>
            )}
            </div>
    );
}

export default Tile;
