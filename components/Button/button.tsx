import React from 'react';
import Link from 'next/link'
import styles from './Button.module.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

library.add(faTimes);
const Button = (props) => {
    const btnStyle = props.buttonType ? styles[`btn--${props.buttonType}`] : styles.btn

    return (
        <>
            {props.href && (
            <Link href={props.href} passHref>
                <div className={btnStyle}>
                    {props.icon && (
                        <FontAwesomeIcon icon={props.icon}/>
                    )}
                    {props.text && (
                        <span>{props.text}</span>
                    )}
                </div>
            </Link>
        )}
            {!props.href && (
                <div className={btnStyle} onClick={() => props.click()}>
                    {props.icon && (
                        <FontAwesomeIcon icon={props.icon}/>
                    )}
                    {props.text && (
                        <span>{props.text}</span>
                    )}
                </div>
            )}
        </>
    );
};

export default Button;

