import {TileTypes} from "./models/interfaces/tile.interface";

export const cards = {
    fact:[
        {
            title: 'Forest Fact',
            type: TileTypes.FACT,
            image: '/sebastian-unrau-sp-p7uuT0tw-unsplash.jpg',
            text: `More than 300 million people live in the forest.
The largest area of forest in the tropics remain the Amazon Basin, amounting to 81.5 million acres.
70% of the world’s animals depends on forests for their homes.
Forests cover 4 billion hectares of the Earth’s surface.`,
            points: 200
        },
        {
            title: 'North Pole Fact',
            type: TileTypes.FACT,
            image: '/pole.jpg',
            text: `From the North Pole, all directions are south. Its latitude is 90 degrees north, and all lines of longitude meet there (as well as at the South Pole, on the opposite end of the Earth). Polaris, the current North Star, sits almost motionless in the sky above the pole, making it an excellent fixed point to use in celestial navigation in the Northern Hemisphere.`,
            points: 200
        }
        ],
    action:[
        {
            title: 'Do Something',
            type: TileTypes.ACTION,
            image: '/wakeup.jpg',
            text: `WAKE UUUUUP`,
            points: 100
        },
        {
            title: 'Write something',
            type: TileTypes.ACTION,
            image: '/writer.jpg',
            text: `Write about anything.`,
            points: 100
        }
    ],
    actor:[
        {
            title: 'Jon Snow',
            type: TileTypes.ACTOR,
            image: '/jonsnow.jpg',
            text: `"You know nothing..."`,
            points: 500
        },
        {
            title: 'Aria Stark',
            type: TileTypes.ACTOR,
            image: '/ariastark.jpg',
            text: `"A girl is Arya Stark of Winterfell, and I am going home."`,
            points: 800
        }
    ],
    quiz:[
        {
            title: 'What time is it, please ?',
            type: TileTypes.QUIZ,
            image: '/quiz.png',
            text: `Quiz will be added soon.`,
            points: 50
        }
    ]
}
