import {AnyAction, Reducer} from "redux";
import {IGame} from "../../models/interfaces/game.interface";
import {TileTypes} from "../../models/interfaces/tile.interface";
import {ICard} from "../../models/interfaces/card.interface";

const INITIAL_STATE: IGame = {
    started: false,
    score: 0,
    position: 0,
    targetPosition: 0,
    card: null,
    tiles: [
        { type: TileTypes.EMPTY },
        { type: TileTypes.FACT },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTION },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTOR },
        { type: TileTypes.EMPTY },
        { type: TileTypes.QUIZ },
        { type: TileTypes.EMPTY },
        { type: TileTypes.FACT },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTION },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTOR },
        { type: TileTypes.EMPTY },
        { type: TileTypes.QUIZ },
        { type: TileTypes.EMPTY },
        { type: TileTypes.FACT },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTION },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTOR },
        { type: TileTypes.EMPTY },
        { type: TileTypes.QUIZ },
        { type: TileTypes.EMPTY },
        { type: TileTypes.FACT },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTION },
        { type: TileTypes.EMPTY },
        { type: TileTypes.ACTOR },
        { type: TileTypes.EMPTY },
        { type: TileTypes.QUIZ }
    ]
}

const game: Reducer = (state = INITIAL_STATE, action: AnyAction): IGame => {


    switch (action.type) {
        case 'ADD_SCORE':
            return {
                ...state,
                score: state.score + action.payload
            }
        case 'SET_TARGET_POSITION':
            let targetPosition = state.position + action.payload
            if (targetPosition > 32) {
                targetPosition = targetPosition - 32
            }
            return {
                ...state,
                targetPosition
            }
        case 'CLOSE_CARD':
            return {
                ...state,
                card: null,
                score: state.score + parseInt(action.payload)
            }
        case 'PLAYER_ADVANCE':
            return {
                ...state,
                position: action.payload
            }
        case 'DRAW_CARD':
            return {
                ...state,
                card: action.payload
            }
        case 'SET_TILES':
            return {
                ...state,
                tiles: action.payload
            }
        case 'SET_POSITION':
            return {
                ...state,
                position: action.payload
            }
        case 'START_GAME':
            return {
                ...state,
                started: true
            }
        case 'END_GAME':
            return {
                ...state,
                started: false
            }
        default:
            return state
    }
}

export default game
