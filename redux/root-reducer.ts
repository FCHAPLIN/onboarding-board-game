import {combineReducers} from "redux";
import user from './user/user-reducer'
import ui from './UI/ui-reducer'
import game from './game/game-reducer'

export default combineReducers({
    ui,
    game,
    user
})
