import {AnyAction, Reducer} from "redux";
import {IUser} from "../../models/interfaces/user.interface";

const INITIAL_STATE: IUser = {
    email: ''
}

const user: Reducer = (state = INITIAL_STATE, action: AnyAction): IUser => {

    switch (action.type) {
        case 'SET_USER':
            return {
                ...state,
                ...action.payload
            }
        case 'DELETE_USER':
            return INITIAL_STATE
        default:
            return state
    }
}

export default user
