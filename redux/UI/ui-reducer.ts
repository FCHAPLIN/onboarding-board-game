import {AnyAction, Reducer} from "redux";
import {BackgroundScroll, IUserInterface} from "../../models/interfaces/user-interface.interface";

const INITIAL_STATE: IUserInterface = {
    loading: false,
    backgroundScroll: BackgroundScroll.TOP
}

const ui: Reducer = (state = INITIAL_STATE, action: AnyAction) => {
    switch (action.type) {
        case 'SET_LOADING':
            return {
                ...state,
                loading: action.payload
            }
        case 'SCROLL_BACKGROUND':
            return {
                ...state,
                backgroundScroll: action.payload
            }
        default:
            return state
    }
}
export default ui
